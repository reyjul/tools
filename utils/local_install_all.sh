#!/bin/bash

USAGE="
usage: $0 [options]

Install locally all tools declared in the repository

Common options
-m PATH      modulefiles path

You can set the MODULEFILES_PATH variable instead of using this option.

Conda tool deployment option :
-c PATH      conda home path

You can set the variable CONDA_HOME instead of using this option.

Singularity tool deployment options :
-i PATH      singularity repository path
-s PATH      software src path

You can set the variables SINGULARITY_REPOSITORY_PATH and/or SOFTWARE_SRC_PATH instead using of these options.

-h           print this help message and exit
"

REQUIREMENTS="
The local_install_all script requires python and the j2cli[yaml] module.
You can setup this environment using conda :
  conda create -n tools -c conda-forge python=3.9.1
  conda activate tools
  pip install j2cli[yaml]
"

while getopts "c:e:hi:m:s:" x; do
    case "$x" in
        h)
            printf "%s\\n" "$USAGE"
            printf "%s\\n" "$REQUIREMENTS"
            exit 2
        ;;
        c)
            export CONDA_HOME="$OPTARG"
            ;;
        i)
            export SINGULARITY_REPOSITORY_PATH="$OPTARG"
            ;;
        m)
            export MODULEFILES_PATH="$OPTARG"
            ;;
        s)
            export SOFTWARE_SRC_PATH="$OPTARG"
            ;;
        ?)
            printf "ERROR: did not recognize option '%s', please try -h\\n" "$x"
            exit 1
            ;;
    esac
done

shift $((OPTIND -1))

if ! command -v j2 &> /dev/null
then
    echo "j2 not found"
    printf "%s\\n" "$REQUIREMENTS"
    exit 1
fi

for tool_path in tools/*/*; do
    if [ -d "$tool_path" ]
    then
      tool_path_array=($(echo "$tool_path" | tr '/' '\n'))
      tool="${tool_path_array[1]}"
      version="${tool_path_array[2]}"
      echo "./utils/local_install.sh $tool $version"
    fi
done
